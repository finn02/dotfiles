#!/bin/bash
# Even though .bash_aliases is meant to store aliases, I'm using it for
# other things as well, since it is sourced in Ubuntu's default .bashrc.

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

:<<-'_aliases_'

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

_aliases_


# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# CP shortcuts by jbixlow(28 August)
:<<-'_docs_'
Longer Version
* for local uses define (-D* : Helps in defining preprocessor directive)
  * -DLOCAL
  * -DLOCAL_PROJECT
  * -DLOCAL_DEBUG
  * -D_GLIBCXX_DEBUG
  * -D_GLIBCXX_DEBUG_PEDANTIC
  * -DNDEBUG :- assertion checking is disabled
* -g : debugging
* -ggdb : debuuging using GNU gdb debugger
* -p : pedantic
* -I : specifiy header file directory
_docs_
# debugging using GNU GDB debuuger
alias gd="g++ -std=gnu++14 -ggdb -DLOCAL";
alias gp="g++ -std=c++14 -pg -DLOCAL";
# Shorter version (-I search header files in specified directory)
# alias g="g++ -O2 -std=c++11 -Wall -Wextra -DLOCAL -Wno-char-subscripts -Wno-unused-result -I /home/jbixlow/contest/"
alias g="g++ -O2 -std=c++14 -Wall -Wextra -pedantic -Wformat=2 -Wfloat-equal -Wlogical-op -Wredundant-decls -Wconversion -Wcast-qual -Wcast-align     -Wuseless-cast -Wno-shadow -Wno-unused-result -Wno-unused-parameter -Wno-unused-local-typedefs -Wno-long-long -g -fsanitize=address,undefined"
alias pc=polygon-cli

# Quickly go backwards in directory tree
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias ......="cd ../../../../.."
alias .......="cd ../../../../../.."
alias cdl="cd \"\`ls -c --group-directories-first | head -n 1\`\""

# Print a message with a system notifier
alias msg="notify-send -u critical"

# System Utilities
alias bat='upower -i /org/freedesktop/UPower/devices/battery_BAT0 | grep -E "state|to\ full|percentage"'

# By jbixlow(31 Aug) - Markdown previewer in terminal
md () {
      pandoc $1 | lynx -stdin
  }
# Github flavoured  markdown parser // gem install github-markdown
gmd() {
    github-markdown $1 | lynx -stdin
}

# Colorful messaging
function red { echo -e "\033[31;1m$@\033[0;m" ; }
function green { echo -e "\033[32;1m$@\033[0;m" ; }
function yellow { echo -e "\033[33;1m$@\033[0;m" ; }
function blue { echo -e "\033[36;1m$@\033[0;m" ; }
function Red { echo -e "\033[31;3m$@\033[0;m" ; }
function Green { echo -e "\033[32;3m$@\033[0;m" ; }
function Yellow { echo -e "\033[33;3m$@\033[0;m" ; }
function Blue { echo -e "\033[36;3m$@\033[0;m" ; }

#some more ls aliases
#alias ll='ls -l'
#alias la='ls -A'
#alias l='ls -CF'
alias fnd="find . -name"
alias sl=""
alias LS=""
alias ack="ack-grep"

#rm related aliases
alias rm='rm -i --preserve-root'

# Grep anything without removing non-matching lines
function color { egrep --color=always "$|$1"; }

# Wrapper for xargs
function sargs { xargs -L1 sh -c "$@" _; }

# Show most place consuming subdirectories
alias dul="du -ahd 1 | sort -rh | head"

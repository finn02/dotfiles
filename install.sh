
#!/bin/bash

# bash
cp -v bashrc
cp -v bash_aliases
cp -v bash_logout

# vim stuff
cp -v vimrc ~/.vimrc

# git stuff
cp -v gitconfig ~/.gitconfig
cp -v gitprompt ~/.gitprompt

# coloring
cp -v .colordiffrc ~/.colordiffrc

# screen
cp -v screenrc ~/.screenrc

# ackrc
cp -v ackrc ~/.ackrc

# set up a basic git bash prompt
if ! grep --quiet parse_git_branch ~/.bashrc; then
    cat .gitprompt >> ~/.bashrc
fi

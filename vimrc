" Vim configuration file

filetype indent plugin on
if has("syntax")
    syntax enable  " enable syntax highlighting
endif
syntax on
set scrolloff=8  " start scrolling screen a few lines before cursor reaches end of screen
set number  " line numbering
set cursorline  " highlight current line
"set relativenumber  " show relative line numbers
set wrap lbr  " soft wrap lines but don't break words
set backspace=indent,eol,start   " make backspace key work as expected

" indentation and tab
set autoindent
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab

" search
set incsearch
set hlsearch  " highlight search matches

set background=dark  " assuming dark background for syntax highlighting
set laststatus=2  " always show status line
set history=1000
set wildmenu
set wildmode=list:longest
set synmaxcol=0   " handles syntax highlighting for really long lines
set mouse=a
" set mouse=v
set showmatch
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/

" cin :  , aw :  , is :  , tm :  , noeb :  , ru :  , cul :  , sy :  , im  
set cin aw ai is ts=4 sw=4 tm=50 nu noeb bg=dark ru cul pastetoggle=<F3>
sy on | im jk <esc> | im kj <esc> | no ; :

" ww : whichwrap, bs : backspace, ts : tabstop, sw : shiftwidth, ai :  , si :  , ci :  , sc :  , nu :  
set nu sc ci si ai sw=4 ts=4 bs=2 ww=<,>,h,l noswapfile
" set whichwrap= b,s,<,>,[,]  "cursor traverse


let &g:makeprg="(g++ -o %:r %:r.cpp -O2 -std=c++14 -Wall && time ./%:r < %:r.in)"
" let &g:makeprg="(g++ -o %:r %:r.cpp -O2 -std=c++14 -Wall -Wextra -pedantic -Wformat=2 -Wfloat-equal -Wlogical-op -Wredundant-decls -Wconversion -Wcast-qual -Wcast-align -Wuseless-cast -Wno-shadow -Wno-unused-result -Wno-unused-parameter -Wno-unused-local-typedefs -Wno-long-long -DLOCAL_PROJECT -g -DLOCAL_DEBUG -D_GLIBCXX_DEBUG -D_GLIBCXX_DEBUG_PEDANTIC -fsanitize=address,undefined)" 
map  <F5> <ESC>:wa<CR>:make!<CR>:copen<CR>
imap <F5> <ESC>:wa<CR>:make!<CR>:copen<CR>

" yank to clipboard
if has("clipboard")
    set clipboard=unnamed
    if has("unnamedplus")
        set clipboard+=unnamedplus
    endif
endif

if has("autocmd")
    " check if file has been modified outside vim
    set autoread
    autocmd CursorHold * call Timer()
    function! Timer()
        call feedkeys("f\e")
        checktime
    endfunction
    set updatetime=5000  " milliseconds
    autocmd FileChangedShell * echo "File changed, type :e! to reload."

    " spell check
    autocmd FileType text set spell spelllang=en_us
    autocmd FileType markdown set spell spelllang=en_us
    autocmd FileType tex set spell spelllang=en_us

    " custom syntax highlighting
    autocmd BufRead,BufNewFile *.pyi set filetype=python
    autocmd BufRead,BufNewFile *.mylog set filetype=diff
    autocmd BufRead,BufNewFile *.cleaver set filetype=markdown
endif

if has("eval")
    " solarized
    let g:solarized_termtrans=1  " use transparent background
    let g:solarized_termcolors=256

    " airline
    let g:airline_powerline_fonts=1  " use better fonts
    let g:airline_exclude_preview=1  " Make airline usable with jedi-vim
    let g:ctags_statusline=1
endif

" enable pathogen
silent! execute pathogen#infect()
silent! colorscheme solarized

#!/bin/bash

echo "************************************************************AUTOCLEAN************************************************************"
sudo apt autoclean

echo "************************************************************UPDATE************************************************************"
sudo apt update -y

echo "************************************************************UPGRADE************************************************************"
sudo apt upgrade -y

echo "************************************************************DIST_UPGRADE************************************************************"
sudo apt full-upgrade # sudo apt-get dist-upgrade

echo "************************************************************FIX_BROKEN_PACKAGES************************************************************"
sudo apt --fix-broken install # sudo apt install -f

echo "************************************************************AUTOREMOVE************************************************************"
sudo apt autoremove


# don't show current checking
#sudo apt-get update -yqq


# searches for program
# apt search
# apt-cache search


# show package details
# apt show
# apt-cache show

# Lists packages with criteria (installed, upgradable etc)
# apt list

# Edits sources list
# apt edit-sources




